package com.crud.react.service;


import com.crud.react.DTO.LoginDto;
import com.crud.react.model.Users;

import java.util.List;
import java.util.Map;

public interface UserService {

    Map<String, Object> login(LoginDto loginDto);

    Users addUsers(Users users);

    List<Users> getAll();

    Users getUsers(Integer id);

    Users editUsers(Integer id , Users users);

    Map<String, Boolean> deleteUsers(Integer id);
}
