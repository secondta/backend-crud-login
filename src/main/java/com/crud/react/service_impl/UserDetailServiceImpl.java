package com.crud.react.service_impl;

import com.crud.react.model.UserPrinciple;
import com.crud.react.model.Users;
import com.crud.react.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       Users users = usersRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("Username tidak ada"));
       return UserPrinciple.build(users);
    }
}
