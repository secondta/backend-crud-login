package com.crud.react.service_impl;

import com.crud.react.DTO.LoginDto;
import com.crud.react.enumated.UserEnum;
import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.jwt.JwtProvider;
import com.crud.react.model.Users;
import com.crud.react.repository.UsersRepository;
import com.crud.react.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UsersRepository usersRepository;

    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    AuthenticationManager authenticationManager;
    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email Or Password Not Found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    @Override
    public Map<String, Object> login(LoginDto loginDto) {
        String token = authories(loginDto.getEmail(), loginDto.getPassword());
        Users user = usersRepository.findByEmail(loginDto.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", user);
        return response;
    }

    @Override
    public Users addUsers(Users users) {
        String email = users.getEmail();
        users.setPassword(passwordEncoder.encode(users.getPassword()));
        if (users.getRole().name().equals("ADMIN"))
            users.setRole(UserEnum.ADMIN);
        else users.setRole(UserEnum.USER);

        var validasi = usersRepository.findByEmail(email);
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf Email sudah digunakan");
        }
        return usersRepository.save(users);
    }

    @Override
    public List<Users> getAll() {
        return usersRepository.findAll();
    }

    @Override
    public Users getUsers(Integer id) {
        return usersRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
    }

    @Transactional
    @Override
    public Users editUsers(Integer id, Users users) {
        Users update = usersRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
        var validasi = usersRepository.findByEmail(users.getEmail());
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf email sudah ada");
        }
        update.setEmail(users.getEmail());
        update.setPassword(users.getPassword());
        return update;

    }

    @Override
    public Map<String, Boolean> deleteUsers(Integer id) {
        try {
            usersRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
