package com.crud.react.service_impl;

import com.crud.react.exception.NotFoundException;
import com.crud.react.model.Products;
import com.crud.react.repository.ProductsRepository;
import com.crud.react.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl implements ProductsService {

    @Autowired
    ProductsRepository productsRepository;

    @Override
    public Products addProducts(Products products) {
        return productsRepository.save(products);
    }

    @Override
    public List<Products> getAll() {
        return productsRepository.findAll();
    }

    @Override
    public Products getProduct(Integer id) {
        return productsRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
    }

    @Transactional
    @Override
    public Products editProduct(Integer id, Products products) {
        Products update = productsRepository.findById(id).orElseThrow(() -> new NotFoundException(("Tidak Ada")));
        update.setNama(products.getNama());
        update.setDeskripsi(products.getDeskripsi());
        update.setImg(products.getImg());
        update.setHarga(products.getHarga());
        return update;
    }

    @Override
    public Map<String, Boolean> deleteProduct(Integer id) {
        try {
            productsRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
