package com.crud.react.controller;

import com.crud.react.model.Products;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductsService productsService;

    @PostMapping
    public CommonResponse<Products> addProduct(@RequestBody Products products) {
        return ResponseHelper.ok(productsService.addProducts(products));
    }
    @GetMapping
    public CommonResponse<List<Products>> getAll() {
        return ResponseHelper.ok(productsService.getAll());
    }
    @GetMapping("/{id}")
    public CommonResponse<Products> getById(@PathVariable("id") Integer id, @RequestBody Products products) {
        return ResponseHelper.ok(productsService.getProduct(id));
    }
    @PutMapping("/{id}")
    public CommonResponse<Products> editProduct(@PathVariable("id") Integer id, Products products) {
        return ResponseHelper.ok(productsService.editProduct(id,products));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteProduct(@PathVariable("id") Integer id){
        return ResponseHelper.ok(productsService.deleteProduct(id));
    }
}
