package com.crud.react.controller;

import com.crud.react.DTO.LoginDto;
import com.crud.react.DTO.UserDto;
import com.crud.react.model.Users;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/sign-in")
    public CommonResponse<Map<String, Object>> login(@RequestBody LoginDto loginDto) {
        return ResponseHelper.ok(userService.login(loginDto));
    }

    @PostMapping("/sign-up")
    public CommonResponse<Users> addUsers(@RequestBody UserDto userDto) {
        return ResponseHelper.ok(userService.addUsers(modelMapper.map(userDto, Users.class)));
    }

    @GetMapping
    public CommonResponse<List<Users>> getAll() {
        return ResponseHelper.ok(userService.getAll());
    }

    @PutMapping("/{id}")
    public CommonResponse<Users> editUsers(@PathVariable("id") Integer id, @RequestBody UserDto userDto) {
        return ResponseHelper.ok(userService.editUsers(id,modelMapper.map(userDto, Users.class)));
    }
    @GetMapping("/{id}")
    public CommonResponse<Users> getUserById(@PathVariable("id") Integer id, @RequestBody Users users) {
        return ResponseHelper.ok(userService.getUsers(id));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteUsers(@PathVariable("id")Integer id) {
        return ResponseHelper.ok(userService.deleteUsers(id));
    }
}
