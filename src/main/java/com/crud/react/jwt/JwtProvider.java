package com.crud.react.jwt;

import io.jsonwebtoken.*;
import io.swagger.models.auth.In;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtProvider {

private static String secretKey = "belajar spring";
private static Integer expired = 900000;

    public String generateToken(UserDetails userDetails) {
        return Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + expired))
                .signWith(SignatureAlgorithm.HS512,secretKey)
                .compact();
    }

    public String getSubject(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJwt(token).getBody().getSubject();
    }
    public boolean checkingTokenJwt(String token) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJwt(token);
            return true;
        } catch (SignatureException e) {
            System.out.println("Invalid JWT signature -> Message:{}");
        } catch (MalformedJwtException e) {
            System.out.println("Invalid JWT Token -> Message : {}");
        } catch (ExpiredJwtException e) {
            System.out.println("Expired JWT Token -> Message: {} ");
        } catch (UnsupportedJwtException e) {
            System.out.println("Unsupported JWT Token -> Message: {}");
        } catch (IllegalArgumentException e) {
            System.out.println("JWT claims string empty-> Message: {}");
        }
        return false;
    }
}
